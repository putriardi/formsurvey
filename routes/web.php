<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\HasilSurveyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('hasilSurvey.login');
});

Route::get('doneSurvey', function () {
    return view('survey.doneSurvey');
});

Route::get('kepuasanPelanggan', [HasilSurveyController::class,'hasilSurvey'])->name('hasil.survey');
Route::get('pengalamanPelanggan', [HasilSurveyController::class,'pengalamanPelanggan']);
Route::post('login', [HasilSurveyController::class,'login'])->name('login.check');
Route::get('logout',[HasilSurveyController::class,'logout'])->name('logout');
Route::get('surveyWa/{no_mr}', [SurveyController::class,'surveyWa']);
Route::get('surveyForm', [SurveyController::class,'storeSurvey']);

Route::resource('survey', SurveyController::class);