<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = file_get_contents("http://10.100.100.43/API/getpas_form.php?nomr=$request->no_mr");
        $kunjungan = json_decode($data);
        //dd($kunjungan);
        if($kunjungan){
            return view('survey.formSurvey', ['kunjungan'=>$kunjungan]);
        }
        else{
            Alert::warning('Cek No MR', 'Data Kunjungan Tidak Ditemukan!');
            return redirect()->back();    
        }
        
    }

    public function surveyWa($no_mr)
    {
        $data = file_get_contents("http://10.100.100.43/API/getpas_form.php?nomr=$no_mr");
        $kunjungan = json_decode($data);
        if($kunjungan){
            $survey = file_get_contents("http://10.100.100.43/API/survey_hist.php?nomr=$no_mr");
            $survey_hist = json_decode($survey);
            if($survey_hist){
                return redirect('doneSurvey');
            }
            else{
                return view('survey.formSurvey', ['kunjungan'=>$kunjungan]);
            }    
        }
        else{
            Alert::warning('', 'Data Kunjungan Tidak Ditemukan!');
            return redirect('/');    
        }
    }

    public function storeSurvey(Request $request)
    {
        $no_mr = $request->no_mr;
        $surv_1 = $request->surv_1;
        $surv_2 = $request->surv_2;
        $surv_3 = $request->surv_3;
        $surv_4 = $request->surv_4;
        $surv_5 = $request->surv_5;
        $surv_6 = $request->surv_6;
        $surv_7 = $request->surv_7;
        $surv_8 = $request->surv_8;
        $surv_9 = $request->surv_9;
        $surv_10 = $request->surv_10;
        $pengalaman = $request->pengalaman;
        
        $survey = file_get_contents("http://10.100.100.43/API/insert_survey.php?no_mr=$no_mr&surv_1=$surv_1&surv_2=$surv_2&surv_3=$surv_3&surv_4=$surv_4&surv_5=$surv_5&surv_6=$surv_6&surv_7=$surv_7&surv_8=$surv_8&surv_9=$surv_9&surv_10=$surv_10&pengalaman=$pengalaman");
        //dd($survey);
        //if($survey == 'sukses'){
            return redirect('doneSurvey')->with('success', 'Survey Berhasil Disimpan!');
        //}

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $model = new Survey;
        // $model->tgl_surv = Carbon::now()->timezone('Asia/Phnom_Penh');
        // $model->no_mr = $request->no_mr;
        // $model->surv_1 = $request->surv_1;
        // $model->surv_2 = $request->surv_2;
        // $model->surv_3 = $request->surv_3;
        // $model->surv_4 = $request->surv_4;
        // $model->surv_5 = $request->surv_5;
        // $model->surv_6 = $request->surv_6;
        // $model->surv_7 = $request->surv_7;
        // $model->surv_8 = $request->surv_8;
        // $model->surv_9 = $request->surv_9;
        // $model->surv_10 = $request->surv_10;
        // $model->pengalaman = $request->pengalaman;
        // dd($model);
        // $model->save();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
