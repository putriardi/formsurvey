<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

    <title>Pelayanan Pelanggan</title>
    <link rel="shortcut icon" href="/images/logo.png" />

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="JSOFT Admin - Responsive HTML5 Template">
		<meta name="author" content="JSOFT.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a class="logo">
						<img src="images/logo-pjnhk.png" height="35"/>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
          <span class="separator"></span>
			
					<ul class="notifications">
					<li>
            <a href="logout" class="notification-icon">
              <i class="fa fa-power-off"></i>
            </a>   
			</li> 		
          </ul>
					<span class="separator"></span>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
					
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									
								<li class="nav-parent nav-expanded nav-active">
										<a>
											<i class="fa fa-list-alt" aria-hidden="true"></i>
											<span>Survey Pelayanan Pelanggan</span>
										</a>
										<ul class="nav nav-children">
											<li class="nav-active">
												<a href="kepuasanPelanggan">
													 Kepuasan Pelanggan
												</a>
											</li>
											<li>
												<a href="pengalamanPelanggan">
													 Pengalaman Pelanggan
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="logout">
											<i class="fa fa-sign-out" aria-hidden="true"></i>
											<span>Log Out</span>
										</a>
									</li>
								</ul>
							</nav>
				
							
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Kepuasan Pelanggan</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a>
										<i class="fa fa-list-alt"></i>
									</a>
								</li>
								<li><span>Kepuasan Pelanggan</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Persyaratan Administrasi</h2>
								</header>
								<div class="panel-body">
									<div id="piechart1" style="height: 400px;"></div>			
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Proses Pendaftaran</h2>
								</header>
								<div class="panel-body">
								<div id="piechart2" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Waktu Pelayanan</h2>
								</header>
								<div class="panel-body">
								<div id="piechart3" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Kewajaran Biaya/Tarif</h2>
								</header>
								<div class="panel-body">
								<div id="piechart4" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Hasil Pemeriksaan</h2>
								</header>
								<div class="panel-body">
								<div id="piechart5" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Fasilitas RS</h2>
								</header>
								<div class="panel-body">
								<div id="piechart6" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Kemampuan Petugas/Pegawai</h2>
								</header>
								<div class="panel-body">
								<div id="piechart7" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Sikap Petugas/Pegawai</h2>
								</header>
								<div class="panel-body">
								<div id="piechart8" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Penanganan Pengaduan</h2>
								</header>
								<div class="panel-body">
								<div id="piechart9" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
									</div>

									<h2 class="panel-title">Pelayanan</h2>
								</header>
								<div class="panel-body">
								<div id="piechart10" style="height: 400px;"></div>	
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							 <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Kalender</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
							</div>
						</div>
					</div>
				</div>
			</aside>
		</section>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data1 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData1; ?>
    ]);

    var options1 = {
	  title: 'Bagaimana persyaratan administrasi yang harus dipenuhi dalam pengurusan pelayanan (rawat jalan atau rawat inap) di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
    var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));

    chart1.draw(data1, options1);
  }
</script>

<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data2 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData2; ?>
    ]);

    var options2 = {
	  title: 'Menurut Bpk/Ibu/Sdr bagaimana proses pendaftaran rawat jalan di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };

    var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));

    chart2.draw(data2, options2);
  }
</script>

<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data3 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData3; ?>
    ]);

    var options3 = {
      title: 'Secara umum, bagaimana jangka waktu yang diperlukan untuk menyelesaikan proses pelayanan di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
    var chart3 = new google.visualization.PieChart(document.getElementById('piechart3'));
    
    chart3.draw(data3, options3);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data4 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData4; ?>
    ]);

    var options4 = {
      title: 'Menurut Bapak/Ibu/sdr, bagaimana kewajaran biaya/tarif yang dikenakan dalam memperoleh pelayanan di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
   
    var chart4 = new google.visualization.PieChart(document.getElementById('piechart4'));
    
    chart4.draw(data4, options4);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data5 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData5; ?>
    ]);
    
    var options5 = {
      title: 'Menurut Bpk/Ibu/Sdr, bagaimana hasil pemeriksaan yang diberikan dari unit layanan?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
    var chart5 = new google.visualization.PieChart(document.getElementById('piechart5'));

    chart5.draw(data5, options5);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data6 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData6; ?>
    ]);
    
    var options6 = {
      title: 'Menurut Bpk/Ibu/Sdr, bagaimana tentang fasilitas (mis. toilet, ruang rawat, ruang tunggu, dll) yang ada di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };

    var chart6 = new google.visualization.PieChart(document.getElementById('piechart6'));
    
	chart6.draw(data6, options6);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data7 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData7; ?>
    ]);
   
	var options7 = {
      title: 'Menurut Bpk/Ibu/Sdr, bagaimana kemampuan (pengetahuan, keahlian, keterampilan) para petugas/pegawai di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
	var chart7 = new google.visualization.PieChart(document.getElementById('piechart7'));
    
	chart7.draw(data7, options7);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data8 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData8; ?>
    ]);
    
	var options8 = {
      title: 'Bagaimana sikap petugas/pegawai di RSJPDHK dalam memberikan pelayanan?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
	var chart8 = new google.visualization.PieChart(document.getElementById('piechart8'));
    
	chart8.draw(data8, options8);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data9 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData9; ?>
    ]);
    
	var options9 = {
      title: 'Menurut Bpk/Ibu/Sdr, bagaimana penanganan pengaduan, saran dan masukan, serta tindak lanjutnya di RSJPDHK?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };
    
	var chart9 = new google.visualization.PieChart(document.getElementById('piechart9'));
    
	chart9.draw(data9, options9);
  }
</script>
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
   var data10 = google.visualization.arrayToDataTable([
      ['Kategori', 'Hasil'],
      <?php echo  $chartData10; ?>
    ]);

    var options10 = {
      title: 'Secara keseluruhan, pendapat Bpk/Ibu/Sdr tentang pelayanan yang telah diberikan?',
      colors: ['#52D726', '#fccf03', '#FF7300', '#FF0000']
    };

    var chart10 = new google.visualization.PieChart(document.getElementById('piechart10'));

    chart10.draw(data10, options10);
  }
</script>
		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="assets/vendor/flot/jquery.flot.js"></script>
		<script src="assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="assets/vendor/raphael/raphael.js"></script>
		<script src="assets/vendor/morris/morris.js"></script>
		<script src="assets/vendor/gauge/gauge.js"></script>
		<script src="assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>

	</body>
</html>